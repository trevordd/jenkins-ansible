= Idea
The "portal" calls a jenkins URL with "buildWithParameters". This is our "API". 
The Jenkins job will use the OCP Pipeline plugin and Ansible to execute the tasks.
Dev tasks will be completed on RH laptops using minishift.

= Pre-req
1. GitLab repo has already been created
2. OCP project has already been created
3. Service account used has admin permissions on OCP/GitLab

= Steps for Greenfield
1. Validate project exists via OCP jenkins plugin
2. Update the OCP template with any properties required via a template language
2. Update the Jenkinsfile template with any properties required via a template language
2. Create pre-reqs within project, i.e.: a OCP template that contains secrets, routes, jenkins, nexus and anything else
3. Deploy template
4. If we are seeding the project, checkout and add in the Jenkinsfile
5. Create "project" within jenkins instance
6. Create multibranch job for git
7. Update gitlab with repo with webhook to jenkins
8. Create a commit to trigger the first build
9. Get the status of the build to validate everything worked

= Tasks
1. Create jenkinsfile to validate project exists
2. Collect templates and resources needed to create everything
3. Get/create a jenkins slave which has ansible included (Gareth - https://gitlab.consulting.redhat.com/gahealy/produban-jenkins-ansible-slave)
4. Work out how to template a file using ginja via ansible
5. Update jenkinsfile to process/create templates
6. Work out how to push a file using ansbile into gitlab (Trevor D)
7. Work out how to create a project in jenkins via ansible
8. Work out how to create a multibranch in jenkins via ansible
9. Work out how to create a webhook in gitlab via ansible
10. 